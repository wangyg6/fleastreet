<font color=gray size=6>[跳蚤街二手校园商城介绍](https://gitee.com/wangyg6/fleastreet/tree/master)</font>

## 一、涉及的工具等
1. [jdk](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)安装 使用1.8
1. 使用[maven](http://maven.apache.org/download.cgi)进行项目依赖管理 具体配置
1. 数据库使用[mysql](https://www.mysql.com/downloads/),管理工具可以参考navicat等
1. 项目基于[SpringBoot](https://projects.spring.io/spring-boot/)，方便快速构建
1. orm框架使用[mybatis](http://www.mybatis.org/mybatis-3/zh/index.html),利于sql拓展，也可以使用spring-data-jpa,本项目使用mybatis
1. 接口[RESTful](http://blog.jobbole.com/41233/)化，与现在趋势接轨。调试可以使用[postman](https://www.getpostman.com/)。
1. 日志使用[logback](https://logback.qos.ch/)
1. 分页插件前端page.js,后端Pagehelper
1. 前端页面使用bootstrap
1. 管理页面使用layui
1. 模板引擎使用thymeleaf
100. 待增加，后续支付等可以接入支付宝、另外减少IO，可以加上使用[redis](https://redis.io/)中间件做缓存,[nginx](http://nginx.org/en/)反向代理做负载均衡。


## 二、功能描述
（功能点梳理，酌情开发，可增删部分功能）
#### 1)用户相关功能
1. 登录功能、注册功能
1. 获取用户登录信息，忘记密码
1. 忘记密码时重置密码开发
1. 更新用户个人信息等


#### 2)商品相关功能
##### 后台页面部分：
1. 分类管理，添加分类，更新分类等
1. 后台商品新增、保存、更新、上架
1. 获取商品详情页等功能开发
1. 后台商品列表页，涉及到分页功能，可以使用插件pageHelper或者硬编码或者不分页等，随意
1. 商品搜索功能开发
1. 商品图片上传等。。

##### 前台页面部分
1. 前台页面详情、列表、搜索、排序等
1. 购物车开发
1. 加入购物车开发
1. 删除、更新购物车功能
1. 全选，单选等
1. 收货地址，增删改查等
1. 支付相关，后续可以接入支付宝或者微信，涉及到token等或者当面交易，可以选择交易地点，联系方式
1. 创建订单
1. 取消订单
1. 订单查询


后续多多思考可能哪些，哪些不好实现，不要也罢




## 三、框架搭建
1. 安装maven，使用aliyun的镜像仓库。IDE中设置maven配置等，一般myeclipse都集成了
1. 安装mysql,修改项目中application.properties中数据库相关配置，包括用户名，密码，端口等
1. 导入项目，以导入maven项目形式导入即可
1. 解决所有问题，摸索运行


## 四、运行
1. 默认端口设置为8888，后续可通过配置修改
2. 通过mvn spring-boot:run或者其他方式运行


#### 后续新增功能点
1. 后端管理系统需要增加权限等级。
2. 找回密码功能




#### 一些说明
1. 执行文件create.sql中sql,于数据库中创建所需要的表,插入所需数据
2. 修改application.properties中数据库配置,用户名密码以及服务端口等
3. 打开FleastreetApplication文件，运行即可
4. 由于product表字段外键受product_type字段约束，而product_type字段外键受product_category字段约束。所以一般product_type和product_category表记录不能删除。如果要想删除，除非关闭外键约束或者把受约束表的相关记录先删除
5. 目前用户分为两种：
    *  后端管理用户，直接写入表中，不支持注册
    *  商城用户，商城页注册，~~后端可以修改删除(已支持新增18.01.29~~)
6. 后端查询展示页，缺失排序，需增加(默认倒序,不过页面仍然不支持排序)
7. 选择产品大类，再选择产品子类  产品add页
8. ~~TODO,04.09日发现点击付款按钮，动画有bug~~
9. ~~目前缺失功能如下：购物车展示页(购物车内容展示)，单个商品详情页(加入购物车),下单后业务流程代码。~~
10. ~~未登录加入购物车的东西，登陆后会同步过去吗？是否有隐藏bug~~










