package com.hbu.fleastreet;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.hbu.fleastreet")
@MapperScan(basePackages = "com.hbu.fleastreet.dao")
@EnableTransactionManagement
public class FleastreetApplication {

	public static void main(String[] args) {
		SpringApplication.run(FleastreetApplication.class, args);
	}
}
