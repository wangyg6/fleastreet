package com.hbu.fleastreet.common;

/**
 * Created by wangyg6 on 2018/1/26.
 * 常量类，用于代码中复用，统一管理
 */
public class Constant {

	public static final Integer ORDER_STATE_0 = 0;//已完成
	public static final Integer ORDER_STATE_1 = 1;//待付款
	public static final Integer ORDER_STATE_2 = 2;//待发货
	public static final Integer ORDER_STATE_3 = 3;//待收货
	public static final Integer ORDER_STATE_CANCELED = -1;//已取消

	public static final Integer STATE_1000 = 1000;//生效
	public static final Integer STATE_1100 = 1100;//失效

	public static final String PREFIX_URL_SAVE_IMAGE = "img/product/";//保存图片路径前缀
	//以下几个数据源于create.sql中初始值定义
	public static final Integer STARTING_VALUE_ORDER =300000;//订单表,记录订单信息  id从300000累加
	public static final Integer STARTING_VALUE_PRODUCT =100000;//创建商品表 id从100000累加

	public static final Integer NO_LOGIN_USER_ID=1111111;//用户没登陆时虚拟id,用于记录加入购物车的信息

	public static final String NAME_PREFIX = "shop_cart_";
}
