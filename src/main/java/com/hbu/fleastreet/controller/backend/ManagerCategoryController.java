package com.hbu.fleastreet.controller.backend;

import com.hbu.fleastreet.entity.ProductCategory;
import com.hbu.fleastreet.entity.ProductType;
import com.hbu.fleastreet.entity.Result;
import com.hbu.fleastreet.entity.ResultCode;
import com.hbu.fleastreet.exception.CategoryException;
import com.hbu.fleastreet.services.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 后端管理页面
 * 分类管理页面
 */
@Controller
@RequestMapping("/manager/category")
public class ManagerCategoryController {
	@Autowired
	ICategoryService categoryService;

	/**
	 * 返回产品大类页
	 * @return
	 */
	@GetMapping("/list")
	public String getCategoryListPage() {
		return "/backend/category/list";
	}

	/**
	 * 打开添加产品大类页面
	 * @return
	 */
	@GetMapping("/add")
	public String getCategoryAddPage() {
		return "/backend/category/add";
	}

	@PostMapping("/list")
	@ResponseBody
	public Result<List<ProductCategory>> getCategoryList() {
		return new Result<List<ProductCategory>>(ResultCode.SUCCESS, categoryService.getCategoryList());
	}

	@GetMapping("/edit/{categoryId}")
	public String editCategory(@PathVariable("categoryId") Integer categoryId, Map<String, Object> map) {
		ProductCategory category = categoryService.findCategoryById(categoryId);
		map.put("category", category);
		return "/backend/category/edit";
	}

	@ResponseBody
	@DeleteMapping("/del/{categoryId}")
	public Result<Boolean> del(@PathVariable("categoryId") int categoryId) {
		try {
			categoryService.delById(categoryId);
		} catch (DataIntegrityViolationException e) {
			return new Result<>(ResultCode.ERROR, "数据完整性异常:" + e.getMessage());
		} catch (Exception e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@PostMapping("/update")
	public Result<Boolean> update(int categoryId, String categoryName, int state) {
		try {
			ProductCategory category = categoryService.findCategoryById(categoryId);
			category.setCategoryName(categoryName);
			category.setState(state);
			categoryService.updateCategory(category);
		} catch (CategoryException e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@PostMapping("/add")
	public Result<Boolean> add(Integer categoryId, String categoryName, int state) {
		ProductCategory category = new ProductCategory();
		category.setCategoryId(categoryId);
		category.setCategoryName(categoryName);
		category.setState(state);
		try {
			categoryService.insertCategory(category);
		} catch (CategoryException e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<>(ResultCode.SUCCESS, true);
	}

	//分隔符——————————————————————————————————————————————————————————————————————————以下为产品子类的相关controller处理

	/**
	 * 返回产品子类页
	 * @return
	 */
	@GetMapping("/type/list")
	public String getTypeListPage() {
		return "/backend/type/list";
	}

	@PostMapping("/type/list")
	@ResponseBody
	public Result<List<ProductType>> getAllProductType() {
		List<ProductType> result = categoryService.getAllProductType();
		return new Result<List<ProductType>>(ResultCode.SUCCESS,result);
	}

	/**
	 * 打开添加产品子类页面
	 * @return
	 */
	@GetMapping("/type/add")
	public String getTypeAddPage() {
		return "/backend/type/add";
	}

	@ResponseBody
	@GetMapping("/type/total")
	public Result<Integer> getTypeTotal() {
		int total = categoryService.countAllType();
		return new Result<>(ResultCode.SUCCESS, total);
	}

	/**
	 * 分页查询
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@PostMapping("/type/list/{pageindex}")
	public Result<List<ProductType>> listProduct(@PathVariable("pageindex") Integer pageindex,
			@RequestParam(defaultValue = "7", value = "pageSize", required = false) Integer pageSize) {
		List<ProductType> list = categoryService.findProductTypeByIndex(pageindex, pageSize).getList();
		return new Result<List<ProductType>>(ResultCode.SUCCESS, list);
	}

	@GetMapping("/type/edit/{typeId}")
	public String editType(@PathVariable("typeId") Integer typeId, Map<String, Object> map) {
		ProductType type = categoryService.findTypeById(typeId);
		ProductCategory category = categoryService.findCategoryByTypeId(typeId);
		map.put("type", type);
		map.put("category", category);
		return "/backend/type/edit";
	}

	@ResponseBody
	@DeleteMapping("/type/del/{typeId}")
	public Result<Boolean> delType(@PathVariable("typeId") int typeId) {
		try {
			categoryService.delTypeById(typeId);
		} catch (DataIntegrityViolationException e) {
			return new Result<>(ResultCode.ERROR, "数据完整性异常:" + e.getMessage());
		} catch (Exception e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@PostMapping("/type/update")
	public Result<Boolean> updateType(int productTypeId, String typeName, int categoryId, int state) {
		try {
			ProductType productType = categoryService.findTypeById(productTypeId);
			productType.setCategoryId(categoryId);
			productType.setState(state);
			productType.setTypeName(typeName);
			categoryService.updateProductType(productType);
		} catch (Exception e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@PostMapping("/type/add")
	public Result<Boolean> addType(Integer typeId, String typeName, Integer categoryId, int state) {
		ProductType productType = new ProductType();
		productType.setProductTypeId(typeId);
		productType.setCategoryId(categoryId);
		productType.setTypeName(typeName);
		productType.setState(state);
		try {
			categoryService.insertProductType(productType);
		} catch (CategoryException e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<>(ResultCode.SUCCESS, true);
	}
}