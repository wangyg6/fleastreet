package com.hbu.fleastreet.controller.backend;

import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.Result;
import com.hbu.fleastreet.entity.ResultCode;
import com.hbu.fleastreet.entity.User;
import com.hbu.fleastreet.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 后端用户管理
 */
@Controller
@RequestMapping("/manager/user")
public class ManagerUserController {
	@Autowired
	private IUserService userService;

	/**
	 * 打开用户列表页面
	 * @return
	 */
	@GetMapping("/list")
	public String list() {
		return "/backend/user/list";
	}

	@GetMapping("/add")
	public String add() {
		return "/backend/user/add";
	}

	@ResponseBody
	@GetMapping("/total")
	public Result<Integer> getTotal() {
		int total = userService.countAll();
		return new Result<>(ResultCode.SUCCESS, total);
	}

	@ResponseBody
	@PostMapping("/list/{pageindex}")
	public Result<List<User>> listProduct(@PathVariable("pageindex") Integer pageindex,
			@RequestParam(defaultValue = "7", value = "pageSize", required = false) Integer pageSize) {
		List<User> list = userService.findUserByIndex(pageindex, pageSize).getList();
		return new Result<List<User>>(ResultCode.SUCCESS, list);
	}

	@GetMapping("/edit/{userId}")
	public String editUser(@PathVariable("userId") Integer userId, Map<String, Object> map) {
		User user = userService.findById(userId);
		map.put("user", user);
		return "/backend/user/edit";
	}

	@ResponseBody
	@DeleteMapping("/del/{userId}")
	public Result<Boolean> del(@PathVariable("userId") int userId) {
		userService.delById(userId);
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@PostMapping("/add")
	@ResponseBody
	//TODO 同一个用户名问题
	public Result<Boolean> addUser(String userName, String password, String email, int state) {
		try {
			//新增
			User user = new User();
			user.setUserName(userName);
			user.setPassword(password);
			user.setEmail(email);
			user.setState(state);
			userService.saveUser(user);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@PostMapping("/update")
	public Result<Boolean> update(int userId, String userName,
			String password, String email,
			int state) {
		try {
			// 更新前先查询
			User user = userService.findById(userId);
			user.setUserId(userId);
			user.setUserName(userName);
			user.setPassword(password);
			user.setEmail(email);
			user.setState(state);
			userService.update(user);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}
}
