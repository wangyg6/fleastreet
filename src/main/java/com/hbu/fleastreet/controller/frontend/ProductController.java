package com.hbu.fleastreet.controller.frontend;

import com.hbu.fleastreet.entity.OrderItem;
import com.hbu.fleastreet.entity.OrderItemDto;
import com.hbu.fleastreet.entity.ProdComment;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductTag;
import com.hbu.fleastreet.entity.Result;
import com.hbu.fleastreet.entity.ResultCode;
import com.hbu.fleastreet.services.ICartService;
import com.hbu.fleastreet.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by linmm on 2017/12/25.
 * 产品页面
 */
@Controller
@RequestMapping("/product")
public class ProductController {
	@Autowired
	IProductService productService;
	@Autowired
	ICartService cartService;

	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Product> list() {
		return productService.getProductList();

	}

	@ResponseBody
	@RequestMapping(value = "/{productId}/detail", method = RequestMethod.GET)
	public Product detail(@PathVariable("productId") Integer productId) {
		return productService.getProductById(productId);
	}

	/**
	 * 用于页面单个商品点击进入详情页
	 * @param productId
	 * @param map
	 * @return
	 */
	@GetMapping("/{productId}")
	public String productDetail(@PathVariable("productId") Integer productId, Map<String, Object> map) {
		Product product = productService.getProductById(productId);
		ProductTag tag = productService.getProductTagById(productId);
		if (tag == null) {
			tag = new ProductTag();
		}
		map.put("product", product);
		map.put("tag", tag);
		return "frontend/single";
	}

	@RequestMapping("/cart")
	public String toCart() {
		return "/frontend/checkout";
	}

	/**
	 * 打开分类查看商品页面
	 *
	 * @return
	 */
	@RequestMapping("/category")
	public String toCatePage(int cid, Map<String, Object> map) {
		return "/frontend/products";
	}

	/**
	 * 分页查询
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@PostMapping("/list/{pageindex}")
	public Result<List<Product>> listProduct(@PathVariable("pageindex") Integer pageindex,
			@RequestParam(defaultValue = "7", value = "pageSize", required = false) Integer pageSize) {
		List<Product> list = productService.findProductByIndexForFront(pageindex, pageSize).getList();
		return new Result<List<Product>>(ResultCode.SUCCESS, list);
	}

	/**
	 * 用于获取分页时总数
	 * @return
	 */
	@ResponseBody
	@GetMapping("/total")
	public Result<Integer> getTotal() {
		int total = productService.countAllForFront();
		return new Result<>(ResultCode.SUCCESS, total);
	}

	/**
	 * 用于按照categoryId获取分页时总数
	 * @return
	 */
	@ResponseBody
	@GetMapping("/total/category/{categoryId}")
	public Result<Integer> getTotalByCategory(@PathVariable("categoryId") Integer categoryId) {
		int total = productService.countAllByCategory(categoryId);
		return new Result<>(ResultCode.SUCCESS, total);
	}

	/**
	 * 用于按照typeId获取分页时总数
	 * @return
	 */
	@ResponseBody
	@GetMapping("/total/type/{typeId}")
	public Result<Integer> getTotalByType(@PathVariable("typeId") Integer typeId) {
		int total = productService.countAllByType(typeId);
		return new Result<>(ResultCode.SUCCESS, total);
	}

	/**
	 * 按categoryId查询总产品分页
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@PostMapping("/list/category/{categoryId}/{pageindex}")
	public Result<List<Product>> listProductByCategory(@PathVariable("categoryId") Integer categoryId,
			@PathVariable("pageindex") Integer pageindex,
			@RequestParam(defaultValue = "12", value = "pageSize", required = false) Integer pageSize) {
		List<Product> list = productService.findProductByIndexAndCategory(categoryId, pageindex, pageSize).getList();
		return new Result<List<Product>>(ResultCode.SUCCESS, list);
	}

	/**
	 * 按typeId查询总产品分页
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@PostMapping("/list/type/{typeId}/{pageindex}")
	public Result<List<Product>> listProductByType(@PathVariable("typeId") Integer typeId,
			@PathVariable("pageindex") Integer pageindex,
			@RequestParam(defaultValue = "12", value = "pageSize", required = false) Integer pageSize) {
		List<Product> list = productService.findProductByIndexAndType(typeId, pageindex, pageSize).getList();
		return new Result<List<Product>>(ResultCode.SUCCESS, list);
	}

	/**
	 * 加购物车
	 * @param productId
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/cart/add")
	public Result<Boolean> addToCart(Integer productId, HttpServletRequest request) throws Exception {
		try {
			cartService.addCart(productId, request);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	/**
	 * 移除购物车
	 * @param productId
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/cart/del")
	public Result<Boolean> delFromCart(Integer productId, HttpServletRequest request) throws Exception {
		try {
			cartService.remove(productId, request);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	/**
	 * 查看购物车商品
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/cart/list")
	public Result<List<OrderItemDto>> listCart(HttpServletRequest request) throws Exception {
		List<OrderItemDto> orderItems = null;
		try {
			orderItems = cartService.listCart(request,false);
		} catch (Exception e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<List<OrderItemDto>>(ResultCode.SUCCESS, orderItems);
	}

	@ResponseBody
	@RequestMapping("/cart/empty")
	public Result<Boolean> emptyCart(HttpServletRequest request) throws Exception {
		cartService.emptyCart(request);
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@RequestMapping("/comment/{productId}")
	public Result<List<ProdComment>> getProdComment(HttpServletRequest request,@PathVariable("productId") Integer productId) throws Exception {
		List<ProdComment> prodComments = null;
		try {
			prodComments = productService.getProdComments(productId);
		} catch (Exception e) {
			return new Result<>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<List<ProdComment>>(ResultCode.SUCCESS, prodComments);
	}

}
