package com.hbu.fleastreet.controller.frontend;

import com.hbu.fleastreet.common.Constant;
import com.hbu.fleastreet.entity.Order;
import com.hbu.fleastreet.entity.OrderItem;
import com.hbu.fleastreet.entity.Result;
import com.hbu.fleastreet.entity.ResultCode;
import com.hbu.fleastreet.entity.User;
import com.hbu.fleastreet.entity.OrderReview;
import com.hbu.fleastreet.services.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by linmm on 2017/12/25.
 * 订单管理
 */
@Controller
@RequestMapping("/order")
public class OrderController {
	@Autowired
	IOrderService orderService;

	@RequestMapping("/hello")
	public String hello(HttpServletRequest request,
			@RequestParam(value = "name", required = false, defaultValue = "springboot-thymeleaf") String name) {
		request.setAttribute("name", name);
		return "hello";
	}

	//请求形如 localhost:8888/order/detail?productId=100001
	@RequestMapping("/detail")
	@ResponseBody
	public OrderItem getOrderItemById(@RequestParam("productId") String productId) {
		return orderService.getOrderItemById(productId);
	}

	@RequestMapping("/all/detail")
	@ResponseBody
	public List<OrderItem> getAllOrder() {
		return orderService.getAllOrderItem();
	}

	//	@RequestMapping("/list.do")
	//	@ResponseBody
	//	public Result<List<OrderList>> listData(HttpServletRequest request) {
	//		List<OrderList> orders = orderService.findUserOrder(request);
	//		return new Result<>(orders);
	@GetMapping("/myorder")
	public String order() {
		return "frontend/orderlist";
	}

	@GetMapping("/list")
	@ResponseBody
	public Result<List<Order>> getOrderList(HttpServletRequest request) {
		List<Order> orderList = null;
		try {
			User user = (User) request.getSession().getAttribute("user");
			orderList = orderService.getAllOrders(user.getUserId());
		} catch (Exception e) {
			new Result<List<Order>>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<List<Order>>(ResultCode.SUCCESS, orderList);
	}

	@GetMapping("/item/{orderId}")
	@ResponseBody
	public Result<List<OrderItem>> getOrderItemsByOrderId(@PathVariable("orderId") Integer orderId) {
		List<OrderItem> orderItems = null;
		try {
			orderItems = orderService.getOrderItemsByOrderId(orderId);
		} catch (Exception e) {
			return new Result<List<OrderItem>>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<List<OrderItem>>(ResultCode.SUCCESS, orderItems);
	}

	@PostMapping("/pay/{orderId}")
	@ResponseBody
	public Result<Boolean> payOrder(@PathVariable("orderId") Integer orderId) {
		List<OrderItem> orderItems = null;
		try {
			orderService.updateOrderStateAndOrderItemState(orderId, Constant.ORDER_STATE_2);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@GetMapping("/orderState/{orderId}")
	@ResponseBody
	public Result<Integer> getOrderStateByOrderId(@PathVariable("orderId") Integer orderId) {
		Integer state = null;
		try {
			state = orderService.getOrderStateByOrderId(orderId);
		} catch (Exception e) {
			return new Result<Integer>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Integer>(ResultCode.SUCCESS, state);
	}

	@ResponseBody
	@PostMapping("/docomment/{orderItemId}/{content}")
	public Result<Boolean> doComment(@PathVariable("orderItemId") Integer orderItemId,
			@PathVariable("content") String content) {
		List<OrderItem> orderItems = null;
		try {
			orderService.insertcomment(orderItemId, content);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@PostMapping("/receive/{orderId}")
	@ResponseBody
	public Result<Boolean> receiveOrder(@PathVariable("orderId") Integer orderId) {
		List<OrderItem> orderItems = null;
		try {
			orderService.updateOrderStateAndOrderItemState(orderId, Constant.ORDER_STATE_0);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@RequestMapping("/submit")
	public Result<Boolean> orderSubmit(HttpServletRequest request, HttpServletResponse response, String name,
			String phone, String addr)
			throws Exception {
		try {
			orderService.submitOrder(request, response, name, phone, addr);
		} catch (Exception e) {
			return new Result<Boolean>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<Boolean>(ResultCode.SUCCESS, true);
	}

	@ResponseBody
	@GetMapping("/comment/{orderItemId}")
	public Result<OrderReview> getComment(HttpServletRequest request, HttpServletResponse response, Integer orderItemId)
			throws Exception {
		OrderReview orderReview = null;
		try {
			orderReview = orderService.getComment(request, response, orderItemId);
		} catch (Exception e) {
			return new Result<OrderReview>(ResultCode.ERROR, e.getMessage());
		}
		return new Result<OrderReview>(ResultCode.SUCCESS, orderReview);
	}
}
