package com.hbu.fleastreet.controller.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by linmm on 2018/1/3.
 * 针对index页面controller
 * 具体内部逻辑的单独提供controller
 * index页面管理
 */
@Controller
@RequestMapping()
public class IndexController {
	/**
	 * 首页
	 * @return
	 */
	@GetMapping({ "/", "/index", "/index.html" })
	public String index() {
		return "frontend/index";
	}





	/**
	 * 错误页
	 * 此出不用写controller,系统默认会从templates/error下取错误页面处理
	 * @return
	 */
/*	@RequestMapping("/error")
	public String error() {
		return "error/404";
	}*/

	/**
	 * 错误页
	 * 这个地方一定不能指定请求类型，比如post,get。
	 * 会出现请求错误转发到该handlerMapping时携带指定请求类型一直错误。导致StackOverFlow
	 * @return
	 */
	@RequestMapping("/error/500")
	public String error500() {
		return "500";
	}

	/**
	 * 联系方式
	 * @return
	 */
	@GetMapping({ "/contact" })
	public String contact() {
		return "frontend/contact";
	}

/*	@GetMapping({ "/products" })
	public String products() {
		return "frontend/products";
	}*/
}

//	/**
//	 * 详情页
//	 * @return
//	 */
//	@GetMapping({ "/single" })
//	public String single() {
//		return "frontend/single";
//	}
//
//	/**
//	 * 注册页
//	 * @return
//	 */
//	@GetMapping({ "/register" })
//	public String register() {
//		return "frontend/register";
//	}
//
//	/**
//	 * 账户页
//	 * @return
//	 */
//	@GetMapping({ "/account" })
//	public String account() {
//		return "frontend/account";
//	}
//
//	/**
//	 * 收银台页面
//	 * @return
//	 */
//	@GetMapping({ "/checkout" })
//	public String checkout() {
//		return "frontend/checkout";
//	}

