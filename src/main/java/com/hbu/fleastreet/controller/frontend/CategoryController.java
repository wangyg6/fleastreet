package com.hbu.fleastreet.controller.frontend;

import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductCategory;
import com.hbu.fleastreet.entity.ProductType;
import com.hbu.fleastreet.entity.Result;
import com.hbu.fleastreet.entity.ResultCode;
import com.hbu.fleastreet.services.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by linmm on 2018/1/5.
 * 产品分类管理,用于分类展示产品页
 * 或者返回分类信息的json
 */
@Controller
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	ICategoryService categoryService;

	/**
	 * 根据typeId,返回所有该typeId下产品信息，用于页面展示
	 * @param typeId
	 * @return
	 */
	@GetMapping(value = "/type/{typeId}")
	public String getProductsByType(@PathVariable("typeId") Integer typeId,Map<String, Object> map) {
		ProductType type = categoryService.findTypeById(typeId);
		map.put("typeId",type.getProductTypeId());
		map.put("name",type.getTypeName());
		return "/frontend/products";
	}

	/**
	 * 返回所有type信息，用于列表展示
	 * @return
	 */
	@PostMapping("/type")
	@ResponseBody
	public String getAllTypes() {
		return null;
	}

	/**
	 * 根据categoryId，返回所有该大类下产品，用于页面展示
	 * @param categoryId
	 * @return
	 */
	@GetMapping("/{categoryId}")
	public String getProductsByCategory(@PathVariable("categoryId") Integer categoryId,Map<String, Object> map) {
		ProductCategory category = categoryService.findCategoryById(categoryId);
		map.put("categoryId",category.getCategoryId());
		map.put("name",category.getCategoryName());
		return "/frontend/products";
	}

	/**
	 * 根据categoryId，返回所有该大类下产品json
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/{categoryId}")
	@ResponseBody
	public Result<List<ProductType>> findTypesByCategory(@PathVariable("categoryId") Integer categoryId) {
		List<ProductType> result = categoryService.findTypesByCategory(categoryId);
		if (result != null && result.size() > 0) {
			return new Result<List<ProductType>>(ResultCode.SUCCESS, result);
		}
		return new Result<List<ProductType>>(ResultCode.ERROR, "未查询到该产品大类");
	}

	/**
	 * 返回所有大分类信息
	 * @param
	 * @return
	 */
	@PostMapping()
	@ResponseBody
	public Result<List<ProductCategory>> getAllCategory() {
		return new Result<List<ProductCategory>>(ResultCode.SUCCESS, categoryService.findAllCategory());
	}

}
