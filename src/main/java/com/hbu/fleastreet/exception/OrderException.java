package com.hbu.fleastreet.exception;

/**
 * Created by wangyg6 on 2018/1/26.
 */
public class OrderException extends BaseException {
	public OrderException(Throwable cause) {
		super(cause);
	}

	public OrderException() {
		super();
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public OrderException(String message) {
		super(message);
	}

	public OrderException(int code, String message) {
		super(code, message);
	}

	public OrderException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}
}
