package com.hbu.fleastreet.exception;

/**
 * Created by linmm on 2018/1/15.
 * 基本异常
 */
public class BaseException extends RuntimeException {
	int code = 9999;
	String msg = "内部异常";

	public BaseException(Throwable cause) {
		super(cause);
	}

	public BaseException() {
		super();
	}

	@Override
	public String toString() {
		return "BaseException{" +
				"code=" + code +
				", msg='" + msg + '\'' +
				'}';
	}

	public BaseException(String message) {
		super(message);
	}

	public BaseException(int code, String message) {
		super(message);
		this.code = code;
	}

	public BaseException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}
}
