package com.hbu.fleastreet.exception;

/**
 * Created by linmm on 2018/1/15.
 */
public class LoginException extends BaseException {
	public LoginException(Throwable cause) {
		super(cause);
	}

	public LoginException() {
	}

	public LoginException(String message) {
		super(message);
	}

	public LoginException(int code, String message) {
		super(code, message);
	}

	public LoginException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}
}
