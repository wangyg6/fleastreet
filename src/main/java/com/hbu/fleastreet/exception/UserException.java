package com.hbu.fleastreet.exception;

/**
 * Created by wangyg6 on 2018/1/24.
 */
public class UserException extends BaseException {
	public UserException(Throwable cause) {
		super(cause);
	}

	public UserException() {
		super();
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public UserException(String message) {
		super(message);
	}

	public UserException(int code, String message) {
		super(code, message);
	}

	public UserException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}
}
