package com.hbu.fleastreet.exception;

/**
 * Created by wangyg6 on 2018/1/24.
 */
public class CategoryException extends BaseException {
	public CategoryException(Throwable cause) {
		super(cause);
	}

	public CategoryException() {
		super();
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public CategoryException(String message) {
		super(message);
	}

	public CategoryException(int code, String message) {
		super(code, message);
	}

	public CategoryException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}
}

