package com.hbu.fleastreet.services;

import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.entity.Order;
import com.hbu.fleastreet.entity.OrderItem;
import com.hbu.fleastreet.entity.OrderReview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by linmm on 2017/12/25.
 */
public interface IOrderService {

	OrderItem getOrderItemById(String productId);

	List<OrderItem> getAllOrderItem();

	int countAll();

	PageInfo<Order> findOrderByIndex(Integer pageindex, Integer pageSize);

	List<OrderItem> getItemByOrderId(int orderId);

	void updateOrderStateAndOrderItemState(int orderId, int state);

	List<Order> getAllOrders(Integer userId);

	List<OrderItem> getOrderItemsByOrderId(Integer orderId);

	void submitOrder(HttpServletRequest request, HttpServletResponse response, String name, String phone, String addr)
			throws Exception;

	void insertcomment(Integer orderItemId, String content)throws Exception;

	Integer getOrderStateByOrderId(Integer orderId)throws Exception;

	OrderReview getComment(HttpServletRequest request, HttpServletResponse response, Integer orderItemId)
			throws Exception;

	/**
	 * 用于获取自增主键，应用于多个关联表
	 * @return
	 */
	int nextVal();
}
