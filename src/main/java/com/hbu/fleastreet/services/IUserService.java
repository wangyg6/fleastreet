package com.hbu.fleastreet.services;

import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.entity.User;

import java.util.List;

/**
 * Created by linmm on 2018/1/16.
 */
public interface IUserService {
	User checkLogin(String username, String password);

	User checkManagerLogin(String username, String password);

	int countAll();

	PageInfo<User> findUserByIndex(Integer pageindex, Integer pageSize);

	User findById(Integer userId);

	void update(User user) throws Exception;

	void delById(int userId);

	void saveUser(User user) throws Exception;

	List<User> checkUserName(String userName);
}
