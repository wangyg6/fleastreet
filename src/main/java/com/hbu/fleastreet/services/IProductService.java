package com.hbu.fleastreet.services;

import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.entity.ProdComment;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductTag;

import java.util.List;

/**
 * Created by linmm on 2017/12/25.
 */
public interface IProductService {

	/**
	 * 获取所有商品，用于生成列表页，展示所有的商品
	 */
	public List<Product> getProductList();

	/**
	 * 获取单个商品信息
	 * @param productId
	 * @return
	 */
	public Product getProductById(Integer productId);

	public ProductTag getProductTagById(Integer productId);

	/**
	 * 分页查询产品信息
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	PageInfo<Product> findProductByIndex(Integer pageindex, Integer pageSize);

	/**
	 * 	表中数据总和
	 */

	int countAll();

	/**
	 * 用于获取自增主键，应用于多个关联表
	 * @return
	 */
	int nextVal();

	/**
	 *新增产品信息
	 * @param pr
	 * @param pt
	 */
	void insertProductAndProductTag(Product pr, ProductTag pt);

	/**
	 * 更新产品信息
	 * @param pr
	 * @param pt
	 */
	void updateProductAndProductTag(Product pr, ProductTag pt);

	void delProductByProductId(int productId);

	/**
	 * 前端展示用查询，需要过滤状态等
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	PageInfo<Product> findProductByIndexForFront(Integer pageindex, Integer pageSize);

	/**
	 * 	表中数据总和，需要过滤状态等
	 */

	int countAllForFront();

	int countAllByCategory(Integer categoryId);

	int countAllByType(Integer typeId);

	PageInfo<Product> findProductByIndexAndCategory(Integer categoryId, Integer pageindex, Integer pageSize);

	PageInfo<Product> findProductByIndexAndType(Integer typeId, Integer pageindex, Integer pageSize);

	/**
	 * 获取商品评价内容
	 * @param productId
	 * @return
	 */
	List<ProdComment> getProdComments(Integer productId);
}
