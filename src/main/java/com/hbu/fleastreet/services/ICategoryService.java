package com.hbu.fleastreet.services;

import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductCategory;
import com.hbu.fleastreet.entity.ProductType;

import java.util.List;

/**
 * Created by linmm on 2018/1/16.
 * 前后端页面共用
 */
public interface ICategoryService {
	/**
	 * 查询所有大类信息：书籍，日用品
	 * @return
	 */
	List<ProductCategory> findAllCategory();

	/**
	 * 根据大类查询细分种类（查询type），比如：书籍>高等数学
	 * @return
	 */
	List<ProductType> findTypesByCategory(Integer categoryId);

	/**
	 * 分页查询分类
	 * @param pageindex
	 * @param pageSize
	 * @return
	 */
	PageInfo<ProductType> findProductTypeByIndex(Integer pageindex, Integer pageSize);

	int countAllType();

	List<ProductCategory> getCategoryList();

	ProductCategory findCategoryById(Integer categoryId);

	void delById(int categoryId);

	void updateCategory(ProductCategory category);

	void insertCategory(ProductCategory category);

	ProductType findTypeById(Integer typeId);

	void delTypeById(int typeId);

	ProductCategory findCategoryByTypeId(Integer typeId);

	void updateProductType(ProductType productType);

	void insertProductType(ProductType productType);

	List<ProductType> getAllProductType();

	/**
	 * 根据类型查询所有商品
	 * @param typeId
	 * @return
	 */
	List<Product> getProductsByTypeId(Integer typeId);

	/**
	 * 根据产品大类查询所有商品
	 * @param categoryId
	 * @return
	 */
	List<Product> getProductsByCategoryId(Integer categoryId);
}
