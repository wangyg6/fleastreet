package com.hbu.fleastreet.services.Impl;

import com.hbu.fleastreet.common.Constant;
import com.hbu.fleastreet.entity.OrderItemDto;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductTag;
import com.hbu.fleastreet.entity.User;
import com.hbu.fleastreet.services.ICartService;
import com.hbu.fleastreet.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by linmm on 2018/4/10.
 */
@Service("cartService")
public class CartServiceImpl implements ICartService {
	@Autowired
	private IProductService productService;

	/**
	 * 将商品id保存到Session中List<Integer>中
	 * @param productId
	 * @param request
	 */
	@Override
	public void addCart(int productId, HttpServletRequest request) throws Exception {
		User loginUser = (User) request.getSession().getAttribute("user");//参见UserController中登陆操作
		if (loginUser == null) {
			loginUser = new User();
			loginUser.setUserId(Constant.NO_LOGIN_USER_ID);
		}
		List<Integer> productIds = (List<Integer>) request.getSession()
				.getAttribute(Constant.NAME_PREFIX + loginUser.getUserId());
		if (productIds == null) {
			productIds = new ArrayList<>();
			request.getSession().setAttribute(Constant.NAME_PREFIX + loginUser.getUserId(), productIds);
		}
		productIds.add(productId);
	}

	/**
	 * 移除session List中对应的商品Id
	 * @param productId
	 * @param request
	 */
	@Override
	public void remove(int productId, HttpServletRequest request) throws Exception {
		User loginUser = (User) request.getSession().getAttribute("user");
		if (loginUser == null) {
			loginUser = new User();
			loginUser.setUserId(Constant.NO_LOGIN_USER_ID);
		}
		List<Integer> productIds = (List<Integer>) request.getSession()
				.getAttribute(Constant.NAME_PREFIX + loginUser.getUserId());
		Iterator<Integer> iterator = productIds.iterator();
		while (iterator.hasNext()) {
			if (productId == iterator.next()) {
				iterator.remove();
			}
		}
	}

	/**
	 * 查询出session的List中所有的商品Id,并封装成List<OrderItem>返回
	 * @param request
	 * @return
	 */
	@Override
	public List<OrderItemDto> listCart(HttpServletRequest request, Boolean needLogin) throws Exception {
		User loginUser = (User) request.getSession().getAttribute("user");
		if (loginUser == null && !needLogin) {
			loginUser = new User();
			loginUser.setUserId(Constant.NO_LOGIN_USER_ID);
		}
		List<Integer> productIds = (List<Integer>) request.getSession()
				.getAttribute(Constant.NAME_PREFIX + loginUser.getUserId());
		// key: productId value:OrderItem
		Map<Integer, OrderItemDto> productMap = new HashMap<Integer, OrderItemDto>();
		if (productIds == null) {
			return new ArrayList<OrderItemDto>();
		}
		// 遍历List中的商品id，每个商品Id对应一个OrderItem
		for (Integer productId : productIds) {
			Product product = productService.getProductById(productId);
			ProductTag productTag = productService.getProductTagById(productId);
			productTag = productTag == null ? new ProductTag() : productTag;
			if (productMap.get(productId) == null) {
				OrderItemDto orderItemDto = new OrderItemDto();
				orderItemDto.setProduct(product);
				orderItemDto.setProductTag(productTag);
				orderItemDto.setProductId(productId);
				orderItemDto.setProductQuantity(1);
				orderItemDto.setSubTotal(product.getPrice());
				productMap.put(productId, orderItemDto);
			} else {
				OrderItemDto orderItemDto = productMap.get(productId);
				int count = orderItemDto.getProductQuantity();
				orderItemDto.setProductQuantity(++count);
				orderItemDto.setSubTotal(orderItemDto.getSubTotal() + product.getPrice());
				productMap.put(productId, orderItemDto);
			}
		}
		List<OrderItemDto> orderItems = new ArrayList<OrderItemDto>(productMap.values());
		return orderItems;
	}

	/**
	 *清空购物车
	 * @param request
	 */
	@Override
	public void emptyCart(HttpServletRequest request) {
		User loginUser = (User) request.getSession().getAttribute("user");
		if (loginUser == null) {
			loginUser = new User();
			loginUser.setUserId(Constant.NO_LOGIN_USER_ID);
		}
		List<Integer> productIds = (List<Integer>) request.getSession()
				.getAttribute(Constant.NAME_PREFIX + loginUser.getUserId());
		if (productIds != null)
			productIds.clear();

	}
}
