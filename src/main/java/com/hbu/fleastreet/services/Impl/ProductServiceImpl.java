package com.hbu.fleastreet.services.Impl;

import ch.qos.logback.classic.Logger;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.common.Constant;
import com.hbu.fleastreet.dao.IProductDao;
import com.hbu.fleastreet.entity.ProdComment;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductTag;
import com.hbu.fleastreet.services.IProductService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by linmm on 2017/12/25.
 */
@Service("productService")
public class ProductServiceImpl implements IProductService {
	private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());
	@Autowired
	private IProductDao productDao;

	@Override
	public List<Product> getProductList() {
		return productDao.queryAll(0, 100);
	}

	@Override
	public Product getProductById(Integer productId) {
		return productDao.queryById(productId);
	}

	@Override
	public ProductTag getProductTagById(Integer productId) {
		return productDao.getProductTagById(productId);
	}

	@Override
	public PageInfo<Product> findProductByIndex(Integer pageindex, Integer pageSize) {
		PageHelper.startPage(pageindex, pageSize);
		List<Product> result = productDao.findProductByIndex();
		PageInfo<Product> productPageInfo = new PageInfo<>(result);
		return productPageInfo;
	}

	@Override
	public int countAll() {
		return productDao.countAll();
	}

	public int nextVal() {
		Integer maxId = productDao.getMaxId();
		if (maxId == null)
			maxId = Constant.STARTING_VALUE_PRODUCT;
		return maxId + 1;
	}

	@Override
	@Transactional
	public void insertProductAndProductTag(Product pr, ProductTag pt) {
		productDao.saveProduct(pr);
		productDao.saveProductTag(pt);
	}

	@Override
	@Transactional
	public void updateProductAndProductTag(Product pr, ProductTag pt) {
		productDao.updateProduct(pr);
		productDao.updateProductTag(pt);
	}

	@Override
	public void delProductByProductId(int productId) {
		productDao.delProductByProductId(productId);
	}



	@Override
	public PageInfo<Product> findProductByIndexForFront(Integer pageindex, Integer pageSize) {
		PageHelper.startPage(pageindex, pageSize);
		List<Product> result = productDao.findProductByIndexForFront();
		PageInfo<Product> productPageInfo = new PageInfo<>(result);
		return productPageInfo;
	}

	@Override
	public int countAllForFront() {
		return productDao.countAllForFront();
	}

	@Override
	public int countAllByCategory(Integer categoryId) {
		return productDao.countAllByCategory(categoryId);
	}

	@Override
	public int countAllByType(Integer typeId) {
		return productDao.countAllByType(typeId);
	}

	@Override
	public PageInfo<Product> findProductByIndexAndCategory(Integer categoryId, Integer pageindex, Integer pageSize) {
		PageHelper.startPage(pageindex, pageSize);
		List<Product> result = productDao.findProductByIndexAndCategory(categoryId);
		PageInfo<Product> productPageInfo = new PageInfo<>(result);
		return productPageInfo;
	}

	@Override
	public PageInfo<Product> findProductByIndexAndType(Integer typeId, Integer pageindex, Integer pageSize) {
		PageHelper.startPage(pageindex, pageSize);
		List<Product> result = productDao.findProductByIndexAndType(typeId);
		PageInfo<Product> productPageInfo = new PageInfo<>(result);
		return productPageInfo;
	}

	@Override
	public List<ProdComment> getProdComments(Integer productId){
		List<ProdComment> prodComments = productDao.getProdComments(productId);
		return prodComments;
	}
}
