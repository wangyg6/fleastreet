package com.hbu.fleastreet.services.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.dao.IUserDao;
import com.hbu.fleastreet.entity.User;
import com.hbu.fleastreet.exception.UserException;
import com.hbu.fleastreet.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linmm on 2018/1/16.
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
	@Autowired
	IUserDao userDao;

	@Override
	public User checkLogin(String username, String password) {
		return userDao.getUserByUserNameAndPassword(username, password);
	}

	@Override
	public User checkManagerLogin(String username, String password) {
		return userDao.getManagerUserByUserNameAndPassword(username, password);
	}

	@Override
	public int countAll() {
		return userDao.countAll();
	}

	@Override
	public PageInfo<User> findUserByIndex(Integer pageindex, Integer pageSize) {
		PageHelper.startPage(pageindex, pageSize);
		List<User> result = userDao.findUserByIndex();
		PageInfo<User> userPageInfo = new PageInfo<User>(result);
		return userPageInfo;
	}

	@Override
	public User findById(Integer userId) {
		return userDao.findById(userId);
	}

	@Override
	public void update(User user) throws Exception {
		try {
			userDao.update(user);
		} catch (Exception e) {
			throw new UserException(e.getMessage());
		}
	}

	@Override
	public void delById(int userId) {
		userDao.delById(userId);
	}

	@Override
	public void saveUser(User user) throws Exception {
		userDao.saveUser(user);
	}

	@Override
	public List<User> checkUserName(String userName) {
		return userDao.checkUserName(userName);
	}

}
