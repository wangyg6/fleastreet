package com.hbu.fleastreet.services.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hbu.fleastreet.dao.ICategoryDao;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductCategory;
import com.hbu.fleastreet.entity.ProductType;
import com.hbu.fleastreet.entity.Result;
import com.hbu.fleastreet.entity.ResultCode;
import com.hbu.fleastreet.exception.CategoryException;
import com.hbu.fleastreet.services.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linmm on 2018/1/16.
 */
@Service("categoryService")
public class CategoryServiceImpl implements ICategoryService {

	@Autowired
	ICategoryDao categoryDao;

	public List<ProductCategory> findAllCategory() {
		return categoryDao.findAllCategory();
	}

	@Override
	public List<ProductType> findTypesByCategory(Integer categoryId) {
		return categoryDao.findTypesByCategory(categoryId);
	}

	@Override
	public PageInfo<ProductType> findProductTypeByIndex(Integer pageindex, Integer pageSize) {
		PageHelper.startPage(pageindex, pageSize);
		List<ProductType> result = categoryDao.findProductTypeByIndex();
		PageInfo<ProductType> productTypePageInfo = new PageInfo<ProductType>(result);
		return productTypePageInfo;
	}

	@Override
	public int countAllType() {
		return categoryDao.countAllType();
	}

	@Override
	public List<ProductCategory> getCategoryList() {
		return categoryDao.getCategoryList();
	}

	@Override
	public ProductCategory findCategoryById(Integer categoryId) {
		return categoryDao.findCategoryById(categoryId);
	}

	@Override
	public void delById(int categoryId) {
		categoryDao.delById(categoryId);
	}

	@Override
	public void updateCategory(ProductCategory category) {
		categoryDao.updateCategory(category);
	}

	@Override
	public void insertCategory(ProductCategory category){
		try {
			categoryDao.insertCategory(category);
		} catch (Exception e) {
			throw new CategoryException(e.getMessage());
		}
	}

	@Override
	public ProductType findTypeById(Integer typeId) {
		return categoryDao.findTypeById(typeId);
	}

	@Override
	public void delTypeById(int typeId) {
		categoryDao.delTypeById(typeId);
	}

	@Override
	public ProductCategory findCategoryByTypeId(Integer typeId) {
		return categoryDao.findCategoryByTypeId(typeId);
	}

	@Override
	public void updateProductType(ProductType productType) {
		categoryDao.updateProductType(productType);
	}

	@Override
	public void insertProductType(ProductType productType) {
		categoryDao.insertProductType(productType);
	}

	@Override
	public List<ProductType> getAllProductType() {
		return categoryDao.getAllProductType();
	}

	@Override
	public List<Product> getProductsByTypeId(Integer typeId) {
		return categoryDao.getProductsByTypeId(typeId);
	}

	@Override
	public List<Product> getProductsByCategoryId(Integer categoryId) {
		return categoryDao.getProductsByCategoryId(categoryId);
	}

}
