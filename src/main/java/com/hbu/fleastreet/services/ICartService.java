package com.hbu.fleastreet.services;

import com.hbu.fleastreet.entity.OrderItemDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by linmm on 2018/4/10.
 */
public interface ICartService {

	/**
	 * 加购物车
	 * @param
	 */
	void addCart(int productId, HttpServletRequest request) throws Exception;

	/**
	 * 移除
	 * @param productId
	 * @param request
	 */
	void remove(int productId, HttpServletRequest request) throws Exception;

	/**
	 * 查看购物车
	 * @param request
	 * @return
	 */
	List<OrderItemDto> listCart(HttpServletRequest request,Boolean needLogin) throws Exception;

	void emptyCart(HttpServletRequest request);
}
