package com.hbu.fleastreet.dao;

import com.hbu.fleastreet.entity.ProdComment;
import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductTag;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by linmm on 2017/12/25.
 */
@Repository("productDao")
public interface IProductDao {

	/**
	 * 减库存
	 * @param productId
	 * @param orderTime
	 * @return 如果影响行数>1，表示更新记录行数
	 */
	//    int reduceNumber(@Param("productId")long productId, @Param("orderTime")Date orderTime);

	/**
	 * 根据id查询商品
	 * @param productId
	 * @return
	 */
	Product queryById(Integer productId);

	/**
	 * 根据偏移量查询商品列表
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Product> queryAll(@Param("offset") int offset, @Param("limit") int limit);

	/**
	 * 获取产品总数，用于页面展示
	 * @return
	 */
	int countAll();

	List<Product> findProductByIndex();

	Integer getMaxId();

	void saveProduct(Product product);

	void saveProductTag(ProductTag pt);

	void updateProduct(Product pr);

	void updateProductTag(ProductTag pt);

	void delProductByProductId(int productId);

	ProductTag getProductTagById(Integer productId);
	int countAllForFront();

	List<Product> findProductByIndexForFront();

	int countAllByCategory(Integer categoryId);

	int countAllByType(Integer typeId);

	List<Product> findProductByIndexAndCategory(Integer categoryId);

	List<Product> findProductByIndexAndType(Integer typeId);

	List<ProdComment> getProdComments(Integer productId);
}
