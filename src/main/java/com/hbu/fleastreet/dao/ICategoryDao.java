package com.hbu.fleastreet.dao;

import com.hbu.fleastreet.entity.Product;
import com.hbu.fleastreet.entity.ProductCategory;
import com.hbu.fleastreet.entity.ProductType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linmm on 2018/1/16.
 */
@Repository("categoryDao")
public interface ICategoryDao {
	/**
	 * 查询所有大类
	 * @return
	 */
	List<ProductCategory> findAllCategory();

	/**
	 * 根据大类查询细分种类
	 * @return
	 */
	List<ProductType> findTypesByCategory(Integer categoryId);

	List<ProductType> findProductTypeByIndex();

	int countAllType();

	List<ProductCategory> getCategoryList();

	ProductCategory findCategoryById(Integer categoryId);

	void delById(int categoryId);

	void updateCategory(ProductCategory category);

	void insertCategory(ProductCategory category);

	ProductType findTypeById(Integer typeId);

	void delTypeById(int typeId);

	ProductCategory findCategoryByTypeId(Integer typeId);

	void updateProductType(ProductType productType);

	void insertProductType(ProductType productType);

	List<ProductType> getAllProductType();

	List<Product> getProductsByTypeId(Integer typeId);

	List<Product> getProductsByCategoryId(Integer categoryId);
}
