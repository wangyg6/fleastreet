package com.hbu.fleastreet.dao;

import com.hbu.fleastreet.entity.Order;
import com.hbu.fleastreet.entity.OrderItem;
import com.hbu.fleastreet.entity.OrderReview;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linmm on 2017/12/25.
 */
@Repository("orderDao")
public interface IOrderDao {

	OrderItem getOrderItemById(String productId);

	List<OrderItem> getAllOrderItem();

	Integer getMaxId();

	int countAll();

	List<Order> findOrderByIndex();

	List<OrderItem> getItemByOrderId(int orderId);

	void updateOrderState(@Param("orderId") int orderId, @Param("state") int state);

	void updateOrderItemState(@Param("orderId") int orderId, @Param("state") int state);

	List<Order> getAllOrders(Integer userId);

	List<OrderItem> getOrderItemsByOrderId(Integer orderId);

	void saveOrderItem(OrderItem orderItem);

	void saveOrder(Order order);

	void insertcomment(@Param("orderItemId")Integer orderItemId, @Param("content")String content)throws Exception;

	Integer getOrderStateByOrderId(Integer orderId);

	OrderReview getCommentbyorderItemId(Integer orderItemId);
}
