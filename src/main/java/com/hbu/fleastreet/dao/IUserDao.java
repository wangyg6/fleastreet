package com.hbu.fleastreet.dao;

import com.hbu.fleastreet.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linmm on 2018/1/16.
 */
@Repository("userDao")
public interface IUserDao {

	User getUserByUserNameAndPassword(@Param("username") String username, @Param("password") String password);

    User getManagerUserByUserNameAndPassword(@Param("username") String username, @Param("password") String password);

	int countAll();

	List<User> findUserByIndex();

	User findById(Integer userId);

	void update(User user);

	void delById(int userId);

	void saveUser(User user);

	List<User> checkUserName(String userName);
}
