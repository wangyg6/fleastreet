package com.hbu.fleastreet.entity;

/**
 * Created by Linmm on 2018/4/16.
 */
public class OrderReview {
	Integer orderItemId;

	String reviewContent;

	Integer start;

	public Integer getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}

	public String getReviewContent() {
		return reviewContent;
	}

	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}
}
