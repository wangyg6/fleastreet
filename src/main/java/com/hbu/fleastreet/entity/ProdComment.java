package com.hbu.fleastreet.entity;

/**
 * Created by Linff on 2018/5/8.
 */
public class ProdComment {
	private Integer prodId;
	private Integer userId;
	private String userName;
	private String reviewContent;

	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}

	public Integer getProdId() {
		return prodId;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getReviewContent() {
		return reviewContent;
	}
}
