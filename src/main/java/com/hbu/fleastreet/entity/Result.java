package com.hbu.fleastreet.entity;

import java.io.Serializable;

/**
 * Created by linmm on 2018/1/16.
 */
public class Result<T> implements Serializable {

	private int code;
	private String msg;
	private T data;

	public Result(ResultCode resultCode, T data) {
		this.code = resultCode.getValue();
		this.msg = resultCode.getMsg();
		this.data = data;
	}

	public Result(ResultCode resultCode, String msg) {
		this.code = resultCode.getValue();
		this.msg = msg;
	}

	public Result(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
