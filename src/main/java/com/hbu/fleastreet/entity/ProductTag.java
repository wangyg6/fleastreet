package com.hbu.fleastreet.entity;

import java.util.Date;

/**
 * Created by linmm on 2018/1/16.
 */
public class ProductTag {
	private Integer productTagId;
	private Integer productId;
	private String productContent;
	private String tagContent;
	private int state;
	private Date createTime;

	public ProductTag() {
	}

	public ProductTag(Integer productId, String productContent, String tagContent,int state) {
		this.productId = productId;
		this.productContent = productContent;
		this.tagContent = tagContent;
		this.state=state;
	}

	public Integer getProductTagId() {
		return productTagId;
	}

	public void setProductTagId(Integer productTagId) {
		this.productTagId = productTagId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductContent() {
		return productContent;
	}

	public void setProductContent(String productContent) {
		this.productContent = productContent;
	}

	public String getTagContent() {
		return tagContent;
	}

	public void setTagContent(String tagContent) {
		this.tagContent = tagContent;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "ProductTag{" +
				"productTagId=" + productTagId +
				", productId=" + productId +
				", productContent='" + productContent + '\'' +
				", tagContent='" + tagContent + '\'' +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProductTag that = (ProductTag) o;

		if (state != that.state)
			return false;
		if (productTagId != null ? !productTagId.equals(that.productTagId) : that.productTagId != null)
			return false;
		if (productId != null ? !productId.equals(that.productId) : that.productId != null)
			return false;
		if (productContent != null ? !productContent.equals(that.productContent) : that.productContent != null)
			return false;
		if (tagContent != null ? !tagContent.equals(that.tagContent) : that.tagContent != null)
			return false;
		return createTime != null ? createTime.equals(that.createTime) : that.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = productTagId != null ? productTagId.hashCode() : 0;
		result = 31 * result + (productId != null ? productId.hashCode() : 0);
		result = 31 * result + (productContent != null ? productContent.hashCode() : 0);
		result = 31 * result + (tagContent != null ? tagContent.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}
}
