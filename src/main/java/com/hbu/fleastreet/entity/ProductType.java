package com.hbu.fleastreet.entity;

import java.sql.Time;
import java.util.Date;

/**
 * Created by linmm on 2018/1/16.
 */
public class ProductType {
	private Integer productTypeId;
	private Integer categoryId;
	private String typeName;
	private int state;
	private Date createTime;

	public Integer getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Integer productTypeId) {
		this.productTypeId = productTypeId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "ProductType{" +
				"productTypeId=" + productTypeId +
				", categoryId=" + categoryId +
				", typeName='" + typeName + '\'' +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProductType that = (ProductType) o;

		if (state != that.state)
			return false;
		if (productTypeId != null ? !productTypeId.equals(that.productTypeId) : that.productTypeId != null)
			return false;
		if (categoryId != null ? !categoryId.equals(that.categoryId) : that.categoryId != null)
			return false;
		if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null)
			return false;
		return createTime != null ? createTime.equals(that.createTime) : that.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = productTypeId != null ? productTypeId.hashCode() : 0;
		result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
		result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}
}
