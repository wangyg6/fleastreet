package com.hbu.fleastreet.entity;

import java.util.Date;

/**
 * Created by linmm on 2018/4/11.
 * orderItem关联产品信息，用于在购物车页展示
 */
public class OrderItemDto {
	private Product product;
	private ProductTag productTag;
	private Integer orderItemId;
	private Integer orderId;
	private Integer productId;
	private int productQuantity;
	private Double subTotal;
	private int state;
	private Date createTime;

	public ProductTag getProductTag() {
		return productTag;
	}

	public void setProductTag(ProductTag productTag) {
		this.productTag = productTag;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		OrderItemDto that = (OrderItemDto) o;

		if (productQuantity != that.productQuantity)
			return false;
		if (state != that.state)
			return false;
		if (product != null ? !product.equals(that.product) : that.product != null)
			return false;
		if (productTag != null ? !productTag.equals(that.productTag) : that.productTag != null)
			return false;
		if (orderItemId != null ? !orderItemId.equals(that.orderItemId) : that.orderItemId != null)
			return false;
		if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null)
			return false;
		if (productId != null ? !productId.equals(that.productId) : that.productId != null)
			return false;
		if (subTotal != null ? !subTotal.equals(that.subTotal) : that.subTotal != null)
			return false;
		return createTime != null ? createTime.equals(that.createTime) : that.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = product != null ? product.hashCode() : 0;
		result = 31 * result + (productTag != null ? productTag.hashCode() : 0);
		result = 31 * result + (orderItemId != null ? orderItemId.hashCode() : 0);
		result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
		result = 31 * result + (productId != null ? productId.hashCode() : 0);
		result = 31 * result + productQuantity;
		result = 31 * result + (subTotal != null ? subTotal.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "OrderItemDto{" +
				"product=" + product +
				", productTag=" + productTag +
				", orderItemId=" + orderItemId +
				", orderId=" + orderId +
				", productId=" + productId +
				", productQuantity=" + productQuantity +
				", subTotal=" + subTotal +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}
}
