package com.hbu.fleastreet.entity;

import java.util.Date;

/**
 * Created by linmm on 2018/1/16.
 */
public class ProductCategory {
	private Integer categoryId;
	private String categoryName;
	private int state;
	private Date createTime;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "ProductCategory{" +
				"categoryId=" + categoryId +
				", categoryName='" + categoryName + '\'' +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProductCategory that = (ProductCategory) o;

		if (state != that.state)
			return false;
		if (categoryId != null ? !categoryId.equals(that.categoryId) : that.categoryId != null)
			return false;
		if (categoryName != null ? !categoryName.equals(that.categoryName) : that.categoryName != null)
			return false;
		return createTime != null ? createTime.equals(that.createTime) : that.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = categoryId != null ? categoryId.hashCode() : 0;
		result = 31 * result + (categoryName != null ? categoryName.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}
}
