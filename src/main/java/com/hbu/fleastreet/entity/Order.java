package com.hbu.fleastreet.entity;

import java.util.Date;

/**
 * Created by wangyg6 on 2018/1/26.
 */
public class Order {
	private Integer orderId;
	private Integer userId;
	private String consignee;
	private String userPhone;
	private String orderArea;
	private Double total;
	private int state;
	private Date createTime;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getOrderArea() {
		return orderArea;
	}

	public void setOrderArea(String orderArea) {
		this.orderArea = orderArea;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Order order = (Order) o;

		if (state != order.state)
			return false;
		if (orderId != null ? !orderId.equals(order.orderId) : order.orderId != null)
			return false;
		if (userId != null ? !userId.equals(order.userId) : order.userId != null)
			return false;
		if (consignee != null ? !consignee.equals(order.consignee) : order.consignee != null)
			return false;
		if (userPhone != null ? !userPhone.equals(order.userPhone) : order.userPhone != null)
			return false;
		if (orderArea != null ? !orderArea.equals(order.orderArea) : order.orderArea != null)
			return false;
		if (total != null ? !total.equals(order.total) : order.total != null)
			return false;
		return createTime != null ? createTime.equals(order.createTime) : order.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = orderId != null ? orderId.hashCode() : 0;
		result = 31 * result + (userId != null ? userId.hashCode() : 0);
		result = 31 * result + (consignee != null ? consignee.hashCode() : 0);
		result = 31 * result + (userPhone != null ? userPhone.hashCode() : 0);
		result = 31 * result + (orderArea != null ? orderArea.hashCode() : 0);
		result = 31 * result + (total != null ? total.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Order{" +
				"orderId=" + orderId +
				", userId=" + userId +
				", consignee='" + consignee + '\'' +
				", userPhone='" + userPhone + '\'' +
				", orderArea='" + orderArea + '\'' +
				", total=" + total +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}
}
