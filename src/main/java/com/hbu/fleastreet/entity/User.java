package com.hbu.fleastreet.entity;

import java.util.Date;

/**
 * Created by linmm on 2018/1/16.
 * 由于数据库中创建的前后端用户表结构一样，所以暂时不写两个模型。用该模型接收数据库中两个表返回的数据
 */
public class User {
	private Integer userId;
	private String userName;
	private String email;
	private String password;
	private int state;
	private Date createTime;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "User{" +
				"userId=" + userId +
				", userName='" + userName + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		User user = (User) o;

		if (state != user.state)
			return false;
		if (userId != null ? !userId.equals(user.userId) : user.userId != null)
			return false;
		if (userName != null ? !userName.equals(user.userName) : user.userName != null)
			return false;
		if (email != null ? !email.equals(user.email) : user.email != null)
			return false;
		if (password != null ? !password.equals(user.password) : user.password != null)
			return false;
		return createTime != null ? createTime.equals(user.createTime) : user.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = userId != null ? userId.hashCode() : 0;
		result = 31 * result + (userName != null ? userName.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}
}
