package com.hbu.fleastreet.entity;

import java.util.Date;

/**
 * Created by wangyg6 on 2018/1/26.
 */
public class OrderItem {
	private Integer orderItemId;
	private Integer orderId;
	private Integer productId;
	private int productQuantity;
	private Double subTotal;
	private int state;
	private Date createTime;
	private OrderReview orderReview;

	public OrderReview getOrderReview() {
		return orderReview;
	}

	public void setOrderReview(OrderReview orderReview) {
		this.orderReview = orderReview;
	}

	public Integer getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		OrderItem orderItem = (OrderItem) o;

		if (productQuantity != orderItem.productQuantity)
			return false;
		if (state != orderItem.state)
			return false;
		if (orderItemId != null ? !orderItemId.equals(orderItem.orderItemId) : orderItem.orderItemId != null)
			return false;
		if (orderId != null ? !orderId.equals(orderItem.orderId) : orderItem.orderId != null)
			return false;
		if (productId != null ? !productId.equals(orderItem.productId) : orderItem.productId != null)
			return false;
		if (subTotal != null ? !subTotal.equals(orderItem.subTotal) : orderItem.subTotal != null)
			return false;
		return createTime != null ? createTime.equals(orderItem.createTime) : orderItem.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = orderItemId != null ? orderItemId.hashCode() : 0;
		result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
		result = 31 * result + (productId != null ? productId.hashCode() : 0);
		result = 31 * result + productQuantity;
		result = 31 * result + (subTotal != null ? subTotal.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "OrderItem{" +
				"orderItemId=" + orderItemId +
				", orderId=" + orderId +
				", productId=" + productId +
				", productQuantity=" + productQuantity +
				", subTotal=" + subTotal +
				", state=" + state +
				", createTime=" + createTime +
				'}';
	}
}
