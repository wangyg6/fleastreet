package com.hbu.fleastreet.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品表，用于记录上架的所有商品
 */
public class Product implements Serializable{

	private Integer productId;
	private String name;
	private int number;
	private Double price;
	private String discount;
	private Integer productTypeId;
	private int state;
	private Date startTime;
	private Date endTime;
	private Date createTime;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public Integer getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Integer productTypeId) {
		this.productTypeId = productTypeId;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Product product = (Product) o;

		if (number != product.number)
			return false;
		if (state != product.state)
			return false;
		if (productId != null ? !productId.equals(product.productId) : product.productId != null)
			return false;
		if (name != null ? !name.equals(product.name) : product.name != null)
			return false;
		if (price != null ? !price.equals(product.price) : product.price != null)
			return false;
		if (discount != null ? !discount.equals(product.discount) : product.discount != null)
			return false;
		if (productTypeId != null ? !productTypeId.equals(product.productTypeId) : product.productTypeId != null)
			return false;
		if (startTime != null ? !startTime.equals(product.startTime) : product.startTime != null)
			return false;
		if (endTime != null ? !endTime.equals(product.endTime) : product.endTime != null)
			return false;
		return createTime != null ? createTime.equals(product.createTime) : product.createTime == null;

	}

	@Override
	public int hashCode() {
		int result = productId != null ? productId.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + number;
		result = 31 * result + (price != null ? price.hashCode() : 0);
		result = 31 * result + (discount != null ? discount.hashCode() : 0);
		result = 31 * result + (productTypeId != null ? productTypeId.hashCode() : 0);
		result = 31 * result + state;
		result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
		result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
		result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
		return result;
	}
}

