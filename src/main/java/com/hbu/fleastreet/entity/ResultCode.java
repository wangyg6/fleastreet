package com.hbu.fleastreet.entity;

/**
 * Created by linmm on 2018/1/16.
 */
public enum ResultCode {

	SUCCESS(0, "成功"), ERROR(-1, "失败");

	int value;
	String msg;

	ResultCode(int value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public int getValue() {
		return value;
	}

}
