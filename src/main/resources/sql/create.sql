-- 数据库初始化脚本

-- 创建数据库
CREATE DATABASE if NOT EXISTS fleastreet;
-- 使用数据库
use fleastreet;
-- 关闭外键约束
SET FOREIGN_KEY_CHECKS=0;

-- 第一部分，状态类建表语句------------------------------------------------
-- 产品状态表以及其他表字段状态标志
drop table if exists `state`;
create table `state` (
  state_id INT NOT NULL  COMMENT '状态id',
  state_name VARCHAR(100) NOT NULL COMMENT '状态描述',
  create_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY(state_id)
)ENgine=innoDB DEFAULT  CHARSET = utf8 comment='产品状态表以及其他表字段状态标志';
INSERT INTO `state`(state_id,state_name) values(1000,'在用'),(1100,'失效,下架');

-- 订单状态表
drop table if exists `order_state`;
create table `order_state` (
  order_state_id TINYINT NOT NULL  COMMENT '订单状态id',
  state_name VARCHAR(100) NOT NULL COMMENT '状态描述',
  create_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (order_state_id)
)ENgine=innoDB DEFAULT  CHARSET = utf8 comment='订单状态编码';
INSERT  INTO `order_state`( `order_state_id`,state_name) values(-1,'已取消'),(0,'已完成'),(1,'待付款')
,(2,'待发货'),(3,'待收货');




--  第二部分，用户类建表语句------------------------------------------------
-- 用户表（后续拓展role,角色控制） id从700000累加
drop table if exists `user`;
create table `user`(
  user_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '用户id',
  user_name VARCHAR(20) NOT NULL  COMMENT '用户名',
  email VARCHAR(50) NOT NULL COMMENT '用户邮箱',
  password VARCHAR(100) NOT NULL COMMENT '用户密码',
  state INT NOT NULL DEFAULT 1000 COMMENT '状态标志,1000 在用 1100 失效',
  create_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (user_id),
  -- 用户名唯一
  KEY idx_user_name(user_name),
  key idx_email(email),
  key idx_password(password),
  CONSTRAINT `fk_state` FOREIGN KEY (`state`) REFERENCES `state` (`state_id`)
)ENgine=innoDB AUTO_INCREMENT=700000 DEFAULT  CHARSET = utf8 comment='用户';
insert INTO `user`(user_name,email,password) VALUES
  ('admin','123@163.com','admin'),
  ('linmm','123@qq.com','linmm');

-- 用户表（后续拓展role,角色控制） id从800000累加
drop table if exists `manager_user`;
create table `manager_user`(
  user_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '用户id',
  user_name VARCHAR(20) NOT NULL  COMMENT '用户名',
  email VARCHAR(50) NOT NULL COMMENT '用户邮箱',
  password VARCHAR(100) NOT NULL COMMENT '用户密码',
  state INT NOT NULL DEFAULT 1000 COMMENT '状态标志,1000 在用 1100 失效',
  role_cd INT NOT NULL DEFAULT 2 COMMENT '用户角色 1-管理员;2-注册用户即商家',
  create_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (user_id),
  -- 用户名唯一
  KEY idx_manager_user_name(user_name),
  key idx_manager_email(email),
  key idx_manager_password(password),
  CONSTRAINT `fk_manager_state` FOREIGN KEY (`state`) REFERENCES `state` (`state_id`)
)ENgine=innoDB AUTO_INCREMENT=800000 DEFAULT  CHARSET = utf8 comment='管理用户';
INSERT INTO fleastreet.manager_user (user_name, email, password, state, create_time, role_cd) VALUES ('admin', '123@163.com', 'admin', 1000, '2018-01-17 09:59:46', 1);
INSERT INTO fleastreet.manager_user (user_name, email, password, state, create_time, role_cd) VALUES ('linmm', '123@qq.com', 'linmm', 1000, '2018-01-17 09:59:46', 2);


-- 第三部分,产品类建表语句-------------------------------------------

-- 产品分类表,表明书籍，电子产品等
drop table IF EXISTS `product_category`;
create table `product_category`(
  category_id BIGINT not null  COMMENT '产品分类id',
  category_name VARCHAR(100) NOT NULL COMMENT '分类名称',
  state INT DEFAULT 1000  COMMENT '状态标志,1000 在用 1100 失效',
  create_time DATETIME  not null  DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (category_id),
  key idx_category_name(category_name),
  CONSTRAINT `fk_category_state` FOREIGN KEY (`state`) REFERENCES `state`(`state_id`)
)ENgine=innoDB DEFAULT  CHARSET = utf8 comment='产品分类,例如书籍,电子产品';
INSERT INTO product_category(category_id,category_name) VALUES
  (101,'书籍'),
  (102,'日用品'),
  (103,'电子产品');




-- 产品类型表,比如T恤,高等数学等细致分类
drop table IF EXISTS `product_type`;
create table `product_type`(
  product_type_id BIGINT NOT NULL COMMENT '产品种类id',
  category_id BIGINT not null COMMENT '产品分类id',
  type_name VARCHAR(100) NOT NULL COMMENT '产品种类名称',
  state INT DEFAULT 1000  COMMENT '状态标志,1000 在用 1100 失效',
  create_time DATETIME  not null  DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (product_type_id),
  key idx_type_name(type_name),
  CONSTRAINT `fk_type_state` FOREIGN KEY (`state`) REFERENCES `state`(`state_id`),
  CONSTRAINT `fk_type_catagory` FOREIGN KEY (`category_id`) REFERENCES `product_category`(`category_id`)
)ENgine=innoDB DEFAULT  CHARSET = utf8  comment='产品详细种类,例如机械类,it类';
insert INTO product_type(product_type_id,category_id,type_name) VALUES
  (1111,101,'公共课类(高数,英语等)'),
  (1112,101,'机械类'),
  (1113,101,'计算机类'),
  (1114,101,'经济类'),
  (1115,101,'外国语类'),
  (1116,101,'医学类'),
  (1117,101,'化学类'),
  (1118,101,'文史哲'),
  (1119,101,'材料高分子'),
  (1120,101,'航空航天'),
  (1121,101,'极品书籍');
insert INTO product_type(product_type_id,category_id,type_name) VALUES
  (1211,102,'短袖'),
  (1212,102,'运动装'),
  (1213,102,'羽绒服'),
  (1214,102,'裤装'),
  (1215,102,'正装'),
  (1216,102,'皮鞋'),
  (1217,102,'运动鞋'),
  (1218,102,'配件'),
  (1219,102,'帽袜类'),
  (1220,102,'极品');
insert INTO product_type(product_type_id,category_id,type_name) VALUES
  (1311,103,'手机平板'),
  (1312,103,'电脑类'),
  (1313,103,'存储类别'),
  (1314,103,'键鼠'),
  (1315,103,'灯具'),
  (1316,103,'乐器'),
  (1317,103,'手办');


-- 创建商品表 id从100000累加
drop table IF EXISTS `product`;
CREATE  TABLE `product`(
  product_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '商品id',
  name VARCHAR(120) NOT NULL COMMENT '商品名称',
  number INT NOT NULL COMMENT '商品数量',
  price DOUBLE NOT NULL COMMENT '商品价格',
  discount VARCHAR(4) NOT NULL  DEFAULT '100%' COMMENT '打折信息',
  -- 可以根据产品种类查出产品大类
  product_type_id BIGINT NOT NULL COMMENT '产品种类id',
  state INT DEFAULT 1000  COMMENT '状态标志,1000 在用 1100 失效',
  manager_user_id  BIGINT NOT NULL DEFAULT 800000 COMMENT '所属商家id',
  start_time datetime NOT NULL DEFAULT current_timestamp COMMENT '上架时间',
  end_time datetime  NOT NULL DEFAULT '30001231' COMMENT  '下架时间',
  create_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY  KEY (product_id),
  KEY idx_prod_name(name),
  KEY idx_start_time(start_time),
  KEY idx_end_time (end_time),
  KEY idx_prod_price(price),
  KEY idx_prod_type(product_type_id),
  CONSTRAINT `fk_product_state` FOREIGN KEY (`state`) REFERENCES `state`(`state_id`),
  CONSTRAINT `fk_prod_user_id` FOREIGN KEY (`manager_user_id`) REFERENCES `manager_user`(`user_id`),
  CONSTRAINT `fk_product_type` FOREIGN KEY (`product_type_id`) REFERENCES `product_type`(`product_type_id`)
)ENGINE =INNODB AUTO_INCREMENT=100001 DEFAULT  CHARSET=utf8 COMMENT='商品表';

INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES (' 同济大学数学系列教材 高等数学 上册 企业批量购书 分享 关注商品举报 同济大学数学系列教材 高等数学 上册', 2, 29.4, '100%', 1111, 1000, '2018-04-04 15:00:49', '3000-12-31 00:00:00', '2018-04-04 15:00:49', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('机械制造技术（第2版）/高等职业教育机械类专业教学用书', 2, 25, '100%', 1112, 1000, '2018-04-04 15:13:57', '3000-12-31 00:00:00', '2018-04-04 15:13:57', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('Springboot从入门到放弃', 2, 12, '100%', 1113, 1000, '2018-04-04 15:15:24', '3000-12-31 00:00:00', '2018-04-04 15:15:24', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('从零开始读懂经济学投资理财书籍家庭理财经济类入门畅销书籍极简经济学经济理论畅销书', 2, 12, '100%', 1114, 1000, '2018-04-04 15:20:16', '3000-12-31 00:00:00', '2018-04-04 15:20:16', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('大学英语四级晨读美文100篇（附赠资深外籍教师朗读光盘+大学英语四级考试大纲词汇表）', 22, 21, '100%', 1115, 1000, '2018-04-09 11:02:44', '3000-12-31 00:00:00', '2018-04-09 11:02:44', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('奈特人体解剖学彩色图谱(第6版)', 334, 12.7, '100%', 1116, 1000, '2018-04-09 11:03:54', '3000-12-31 00:00:00', '2018-04-09 11:03:54', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('哲学简史/诺贝尔文学奖获得者伯特兰·罗素写给大众的哲学入门读物', 34, 21.6, '100%', 1118, 1000, '2018-04-09 11:04:39', '3000-12-31 00:00:00', '2018-04-09 11:04:39', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('基础有机化学(第4版)上册', 667, 18.9, '100%', 1117, 1000, '2018-04-09 11:05:44', '3000-12-31 00:00:00', '2018-04-09 11:05:44', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('高分子材料（第2版）', 999, 31.1, '100%', 1119, 1000, '2018-04-09 11:07:24', '3000-12-31 00:00:00', '2018-04-09 11:07:24', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('航空航天飞行器动力学建模与仿真（第3版）', 21, 23.7, '100%', 1120, 1000, '2018-04-09 11:12:21', '3000-12-31 00:00:00', '2018-04-09 11:12:21', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('瑞比卡(Marc Rebecca)桑蚕丝印花短裙套头荷叶边短袖连衣裙 82045E 红色印花 XL', 23, 589, '100%', 1211, 1000, '2018-04-09 11:15:26', '3000-12-31 00:00:00', '2018-04-09 11:15:26', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('朗悦女装 2018春季新款韩版时尚运动休闲卫衣套装修身卫裤两件套 LWWY181238 藏蓝色 L', 12, 199, '100%', 1212, 1000, '2018-04-09 11:17:16', '3000-12-31 00:00:00', '2018-04-09 11:17:16', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('Canada Goose 全球购加拿大鹅男士羽绒大衣Expedition系列羽绒服 红色Red L', 2, 9600, '100%', 1213, 1000, '2018-04-09 11:19:06', '3000-12-31 00:00:00', '2018-04-09 11:19:06', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('AK男装 （AKSERIES）都市特工多分割胶印拉链袋收脚口修身运动长裤卫裤男1752027 炭花灰 XL', 3, 399, '100%', 1214, 1000, '2018-04-09 11:21:00', '3000-12-31 00:00:00', '2018-04-09 11:21:00', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('NUMAXSEN轻奢品牌进口羊毛西服套装男商务休闲修身职业正装职业西装2018新款 藏青 42', 1, 5988, '100%', 1215, 1000, '2018-04-09 11:22:21', '3000-12-31 00:00:00', '2018-04-09 11:22:21', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('S.T.Dupont/都彭男鞋正品 进口鲨鱼皮商务正装皮鞋 RUBKIN底 舒适耐磨鞋 黑色E16211477 39', 2, 7550, '100%', 1216, 1000, '2018-04-09 11:23:41', '3000-12-31 00:00:00', '2018-04-09 11:23:41', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('HERMES爱马仕皮带蓝珐琅扣黑/浅棕皮质两面腰带宽32mm 105cm', 1, 8585, '100%', 1218, 1000, '2018-04-09 11:25:09', '3000-12-31 00:00:00', '2018-04-09 11:25:09', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('阿迪达斯adidas男女袜子运动休闲棉袜六双装黑白色M码39-42码', 33, 216, '100%', 1219, 1000, '2018-04-09 11:26:45', '3000-12-31 00:00:00', '2018-04-09 11:26:45', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('阿迪达斯Adidas 三叶草 2018 男 TUBULAR DOOM SOCK小椰子轮胎底潮流运动袜子休闲跑步鞋 CG5509 42码', 2, 1399, '100%', 1217, 1000, '2018-04-09 11:30:06', '3000-12-31 00:00:00', '2018-04-09 11:30:06', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('Apple iPad Pro 平板电脑 12.9英寸（512G WLAN+Cellular版/A10X/Retina屏/Multi-Touch MPLX2CH/A）深空灰色', 5, 10288, '100%', 1311, 1000, '2018-04-09 11:31:35', '3000-12-31 00:00:00', '2018-04-09 11:31:35', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('【移动专享版】Apple iPhone X (A1865) 64GB 深空灰色 移动联通电信4G手机', 3, 8388, '100%', 1311, 1000, '2018-04-09 11:33:02', '3000-12-31 00:00:00', '2018-04-09 11:33:02', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('微软（Microsoft）Surface Laptop超轻薄触控笔记本（13.5英寸 i5-7200U 8G 256GSSD Windows10S）灰钴蓝', 2, 8988, '100%', 1312, 1000, '2018-04-09 11:34:27', '3000-12-31 00:00:00', '2018-04-09 11:34:27', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('三星(SAMSUNG) T5系列 500G 移动固态硬盘（MU-PA500B/CN）', 1299, 1299, '100%', 1313, 1000, '2018-04-09 11:35:40', '3000-12-31 00:00:00', '2018-04-09 11:35:40', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('雷蛇（RAZER） 堡垒神蛛Turret 蓝牙连接2.4G无线游戏鼠标键盘套装', 798, 1299, '100%', 1314, 1000, '2018-04-09 11:38:03', '3000-12-31 00:00:00', '2018-04-09 11:38:03', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('米家（MIJIA）小米生态链LED写字灯 阅读台灯智能台灯 任意调光 APP控制 灯光场景模拟', 123, 150, '100%', 1315, 1000, '2018-04-09 11:40:50', '3000-12-31 00:00:00', '2018-04-09 11:40:50', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('FENDER 923-6007-249 62TELE Custom Shop限量做旧电吉他', 1, 29999, '100%', 1316, 1000, '2018-04-09 11:41:55', '3000-12-31 00:00:00', '2018-04-09 11:41:55', 800000);
INSERT INTO fleastreet.product (NAME, number, price, discount, product_type_id, state, start_time, end_time, create_time, manager_user_id) VALUES ('和泉纱雾 Q版黏土人 可换脸模型玩偶手办 妹妹 漫画老师模型公仔', 66, 99, '100%', 1317, 1000, '2018-04-09 11:42:58', '3000-12-31 00:00:00', '2018-04-09 11:42:58', 800000);


-- 产品标签表,记录具体产品描述 id从200000递增
drop table IF EXISTS `product_tag`;
create table `product_tag`(
  product_tag_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '标签id',
  product_id BIGINT NOT NULL COMMENT '产品id',
  product_content VARCHAR(2400)  COMMENT '内容描述',
  tag_content VARCHAR(200)  COMMENT '标签信息,以分号分割',
  state INT  NOT NULL DEFAULT 1000  COMMENT '状态标志,1000 在用 1100 失效',
  create_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (product_tag_id),
  KEY idx_product_id(product_id),
  -- 由于索引长度限制，所以不对product_content建立索引
  key idx_tag_content(tag_content),
  CONSTRAINT `fk_tag_state` FOREIGN KEY (`state`) REFERENCES `state`(`state_id`)
)ENgine=innoDB DEFAULT  CHARSET = utf8 AUTO_INCREMENT=200000 comment='产品标签表,记录每个产品的关键字';

INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100005, '北京外国语大学、北京第二外国语大学、天津外国语大学、上海外国语大学、西安外国语大学等十五所全国高校教师倾力推荐！随书附赠手机二维码扫描下载音频！', '出版社： 中国宇航出版社 ISBN：9787515905327版次：1商品编码：11381329包装：平装丛书名： 考试必备 开本：32开出版时间：2014-01-01用纸：胶版纸页数：432字数：376000正文语种：中文，英文附件：光盘附件数量：1', 1000, '2018-04-09 11:02:44');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100006, '医学界的米开朗琪罗——奈特博士亲手绘制，并补充多位医学插画师的绘制内容，500余幅精美全彩中英文双语铜版纸图谱', '出版社： 人民卫生出版社 ISBN：9787117210294版次：6商品编码：11758830包装：平装开本：16开出版时间：2015-07-01用纸：铜版纸页数：655', 1000, '2018-04-09 11:03:54');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100007, '20世纪伟大的思想家罗素带领你走进哲学的殿堂。梳理哲学发展的脉络，解读近百位哲学大师的智慧。通俗易懂，是了解西方文化的入门书。', '伯特兰·罗素 著', 1000, '2018-04-09 11:04:39');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100008, '邢其毅，裴伟伟，徐瑞秋，裴坚 著', '邢其毅，裴伟伟，徐瑞秋，裴坚，北京大学化学与分子工程学院教授,知名有机化学家。编写的《基础有机化学》各版次在国内享有盛誉。', 1000, '2018-04-09 11:05:44');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100009, '高分子材料（第2版）
企业批量购书
分享 关注商品举报
高分子材料（第2版）
黄丽 编', '测试', 1000, '2018-04-09 11:07:24');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100010, '彼得·H.基普斐（Peter H.Zipfal） 著；高亚奎，郭圣洪，辛长范 等 译', '出版社： 航空工业出版社 ISBN：9787516511381版次：3商品编码：12118260包装：平装丛书名： 飞机设计技术丛书 开本：16开出版时间：2017-01-01用纸：胶版纸页数：469字数：703000正文语种：中文', 1000, '2018-04-09 11:12:21');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100011, '瑞比卡（Marc Rebecca）', '商品名称：瑞比卡连衣裙商品编号：6798039商品毛重：500.00g商品产地：中国深圳货号：82045E腰型：中腰风格：名媛淑女厚度：适中领型：圆领颜色：红色系适用年龄：25-29周岁图案：其它尺码：S，M，L，XL组合规格：单件裙型：荷叶边裙主要材质：蚕丝流行元素：印花袖长：短袖衣门襟：套头版型：修身型袖型：常规裙长：短裙分类：短袖连衣裙适用人群：胖mm，少女，轻', 1000, '2018-04-09 11:15:26');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100012, '商品名称：LAXJOY朗悦卫衣商品编号：6460230商品毛重：0.7kg商品产地：中国浙江货号：LWWY181238风格：休闲厚度：适中领型：圆领颜色：黑色系，灰色系适用年龄：18-24周岁图案：其它尺码：S，M，L，XL组合规格：上下装版型：宽松型主要材质：聚酯袖长：长袖衣门襟：套头袖型：其它款式：套头衣长：常规款', 'LAXJOY朗悦', 1000, '2018-04-09 11:17:16');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100013, '商品名称：Canada Goose 全球购加拿大鹅男士羽绒大衣Expedition系列羽绒服 红色Red L商品编号：17932909210店铺： 隆恩美国购物网商品毛重：2.0kg货号：4565M尺码：XS，S，M，L，XL，XXL内部填充物：其它上市时间：17年秋冬功能：保暖，防风，防水，透气，其它分类：羽绒服适用人群：男士面料：其它', '商品美国发货，到货时间3 - 4周左右', 1000, '2018-04-09 11:19:06');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100014, '品牌： AKSERIES
商品名称：AKSERIES1752027商品编号：6329371商品毛重：0.54kg商品产地：中国广州市货号：1110411752027106腰型：中腰厚度：常规适用场景：其它裤型：修身裤裤长：长裤适用季节：四季基础风格：青春流行主要材质：棉适用人群：青少年上市时间：2017春季', '急速配送', 1000, '2018-04-09 11:21:00');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100015, '品牌： 纽曼森（Numaxsen）
商品名称：NUMAXSEN轻奢品牌进口羊毛西服套装男商务休闲修身职业正装职业西装2018新款 藏青 42商品编号：23998843869店铺： 纽曼森男装旗舰店商品毛重：1.0kg商品产地：中国大陆货号：T010612领宽：规则领型（7-9cm）开叉：双开叉版型：修身型主要材质：羊毛领型：平驳领流行元素：多口袋厚度：常规颜色：灰色基础风格：商务绅士衣门襟：两粒单排扣风格：商务正装面料：毛呢适用季节：春季适用场景：上班，商务适用人群：中年上市时间：2018春季', '测试用', 1000, '2018-04-09 11:22:21');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100016, '品牌： 都彭（S.T.Dupont）
商品名称：S.T.Dupont/都彭男鞋正品 进口鲨鱼皮商务正装皮鞋 RUBKIN底 舒适耐磨鞋 黑色E16211477 39商品编号：1716478402店铺： 雅博鞋类专营店商品毛重：1.5kg货号：E16211477尺码：37销售渠道类型：专柜同款风格：简约鞋面材料：鳄鱼皮内里材质：皮颜色：黑色鞋跟形状：有跟图案：纯色鞋头款式：圆头闭合方式：系带', '法国都彭，商场同款，支持验货，7天退换，赠送运费险', 1000, '2018-04-09 11:23:41');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100017, '商品名称：HERMES爱马仕皮带蓝珐琅扣黑/浅棕皮质两面腰带宽32mm 105cm商品编号：1955288705店铺： CHEER奢侈品全球购专营店商品毛重：1.0kg货号：蓝珐琅扣黑/浅棕皮质两面腰带适用人群：男式风格：商务休闲类型：板扣', '忠于传统手工艺 至精至美 满减优惠 猛戳！', 1000, '2018-04-09 11:25:09');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100018, '品牌： 阿迪达斯（adidas）
商品名称：阿迪达斯AA2285+AA2286商品编号：4681364商品毛重：240.00g商品产地：土耳琪货号：AA2285+AA2286尺码：39-42厚度：中厚筒高：中筒袜颜色：白色系，黑色系材质：棉花型：纯色组合形式：5-6双装适用季节：四季袜子功能用途：运动适用人群：情侣上市时间：2017冬季', '测试', 1000, '2018-04-09 11:26:45');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100019, '品牌： 阿迪达斯（Adidas）
商品名称：阿迪达斯跑步鞋商品编号：6755584商品毛重：1.0kg商品产地：中国大陆货号：CG5509尺码：40，40.5，41，42，42.5，43，44，44.5帮面材质：织物，网布选购热点：经典款闭合方式：系带适合路面：跑道，公路，小道鞋底材质：天然橡胶上市时间：2018春季功能：缓冲，避震，透气，轻质适用人群：男士适用场景：休闲，运动，户外', '测试过', 1000, '2018-04-09 11:30:06');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100020, '品牌： Apple
商品名称：AppleiPad Pro 12.9英寸商品编号：5173443商品毛重：0.692kg商品产地：中国大陆系统：ios系统硬盘：512G及以上分辨率：超高清屏（2K/3K/4K）尺寸：12.1英寸及以上分类：娱乐平板裸机重量：501g-700g厚度：7.0mm以下', '【四月春季特惠】2018年新品发布会已结束，iPad Pro办公娱乐学习三不误，要买就现在！', 1000, '2018-04-09 11:31:35');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100021, 'iPhone X 全面屏，科技全面绽放。数量有限，赶紧下单！
流量日租卡 1元=1GB/天 不用不花钱，可自动叠加', '
分辨率：2436×1125
后置摄像头：1200万像素
前置摄像头：700万像素
核      数：其他
频      率：以官网信息为准
品牌： Apple
商品名称：AppleiPhone X商品编号：5531825商品毛重：430.00g商品产地：中国大陆运营商：移动合约机', 1000, '2018-04-09 11:33:02');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100022, '商品名称：微软Surface Laptop商品编号：4642183商品毛重：2.33kg商品产地：中国大陆系统：Windows 10厚度：10.0mm—15.0mm内存容量：8G分辨率：超高清屏（2K/3k/4K)显卡型号：其他待机时长：9小时以上处理器：Intel i5低功耗版特性：窄边框，触控屏，背光键盘显卡类别：集成显卡裸机重量：1-1.5KG硬盘容量：256G固态显存容量：其他分类：轻薄本屏幕尺寸：其他', '可触控2K屏 C面采用高端跑车内饰欧缔兰材料', 1000, '2018-04-09 11:34:27');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100023, '品牌： 三星（SAMSUNG）
商品名称：三星MU-PA500B/CN商品编号：4718625商品毛重：190.00g商品产地：韩国尺寸：1.8英寸接口：USB3.1容量：500G及以下类型：SSD固态移动硬盘', '三星移动固态硬盘，金属外观，更小体积，更快传输！', 1000, '2018-04-09 11:35:40');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100024, '商品名称：雷蛇（RAZER） 堡垒神蛛Turret 蓝牙连接2.4G无线游戏鼠标键盘套装商品编号：10708606218店铺： 雷蛇外设旗舰店商品毛重：1.6kg商品产地：中国大陆货号：RZ84-01330100-B3A1背光效果：无，单色背光类型：键鼠套装，薄膜键盘，数字键盘连接方式：无线游戏性能：入门级轴体类型：其他数字键盘：无，有数字键盘', '创新型客厅式游戏外设解决方案，蓝牙+2.4G无线！', 1000, '2018-04-09 11:38:03');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100025, '品牌： 米家（MIJIA）
商品名称：米家LED写字灯商品编号：3007224商品毛重：1.48kg商品产地：中国大陆功能：远程开/关，白光分类：智能氛围/照明', '无', 1000, '2018-04-09 11:40:50');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100026, '品牌： FENDER
商品名称：FENDER 923-6007-249 62TELE Custom Shop限量做旧电吉他商品编号：26564089669店铺： 汇千乐器专营店商品毛重：5.0kg颜色：蓝色背侧板材质：其它分类：电吉他指板材质：玫瑰木圆角/缺角：缺角面板材质：梣木规格：41寸', '附带原装琴盒 一年保修', 1000, '2018-04-09 11:41:55');
INSERT INTO fleastreet.product_tag (product_id, product_content, tag_content, state, create_time) VALUES (100027, '店铺： 漫宇影视周边专营店商品编号：22569575615
', '春季焕新 满99立减10元 满199减20元', 1000, '2018-04-09 11:42:58');




-- 第四部分，订单类建表语句------------------------------------------------
-- 订单表,记录订单信息  id从300000累加
drop table IF EXISTS `order`;
create table `order`(
  order_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '订单id',
  user_id BIGINT NOT NULL  COMMENT '用户id',
  consignee varchar(255) NOT NULL COMMENT '收货人',
  user_phone VARCHAR(18) NOT NULL COMMENT '手机号',
  order_area VARCHAR(400) NOT NULL COMMENT '设置的交易地点,例如二号教学楼304室',
  total  double NOT NULL COMMENT '总价',
  manager_user_id  BIGINT NOT NULL DEFAULT 800000 COMMENT '所属商家id',
  state TINYINT not null default 1 comment '(-1,''已取消''),(0,''已完成''),(1,''待付款'')
,(2,''待发货''),(3,''待收货'')',
  create_time DATETIME  not null  DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (order_id),
  key idx_create_time(create_time),
  key idx_user_id(user_id),
  CONSTRAINT `fk_order_state` FOREIGN KEY (`state`) REFERENCES `order_state`(`order_state_id`),
  CONSTRAINT `fk_order_user_id` FOREIGN KEY (`manager_user_id`) REFERENCES `manager_user`(`user_id`)
)ENgine=innoDB DEFAULT  CHARSET = utf8 AUTO_INCREMENT=300000 comment='订单信息';

INSERT INTO fleastreet.`order` (user_id, consignee, user_phone, order_area, total, state, create_time, manager_user_id) VALUES (700004, 'linfeifei', '188888888', '测试用地址', 27525, 0, '2018-04-11 18:17:26', 800000);
INSERT INTO fleastreet.`order` (user_id, consignee, user_phone, order_area, total, state, create_time, manager_user_id) VALUES (700004, 'wangyueg', '19999', '没地址', 3014, 0, '2018-04-11 18:18:17', 800000);
INSERT INTO fleastreet.`order` (user_id, consignee, user_phone, order_area, total, state, create_time, manager_user_id) VALUES (700018, '林芳芳', '199999', '合肥', 1449, 3, '2018-04-11 18:26:16', 800000);
INSERT INTO fleastreet.`order` (user_id, consignee, user_phone, order_area, total, state, create_time, manager_user_id) VALUES (700004, 'wangyueguo', '19999', 'shanghai', 30098, 0, '2018-04-11 18:27:06', 800000);
INSERT INTO fleastreet.`order` (user_id, consignee, user_phone, order_area, total, state, create_time, manager_user_id) VALUES (700020, 'hehe', 'hehe', 'hehe', 1449, 0, '2018-04-11 18:34:44', 800000);
INSERT INTO fleastreet.`order` (user_id, consignee, user_phone, order_area, total, state, create_time, manager_user_id) VALUES (700017, 'linff', '123', '上海', 150, 0, '2018-04-12 11:48:54', 800000);
-- 订单内容表,记录订单内商品信息  id从400000累加
drop table IF EXISTS `order_item`;
create table `order_item`(
  order_item_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '订单项id',
  order_id BIGINT NOT NULL  COMMENT '订单id',
  product_id BIGINT not null comment '商品id',
  product_quantity int not null COMMENT '商品数量',
  sub_total double NOT NULL COMMENT '单个商品总价',
  state TINYINT not null default 1 comment '(-1,''已取消''),(0,''已完成''),(1,''待付款'')
,(2,''待发货''),(3,''待收货'')',
  create_time DATETIME  not null  DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (order_item_id),
  key idx_order_id(order_id),
  key idx_order_product_id(product_id),
  CONSTRAINT `fk_order_item_state` FOREIGN KEY (`state`) REFERENCES `order_state`(`order_state_id`)
)ENgine=innoDB DEFAULT  CHARSET = utf8 AUTO_INCREMENT=400000 comment='订单信息';
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300001, 100016, 1, 7550, 0, '2018-04-11 18:17:26');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300001, 100021, 1, 8388, 0, '2018-04-11 18:17:26');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300001, 100020, 1, 10288, 0, '2018-04-11 18:17:26');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300001, 100024, 1, 1299, 0, '2018-04-11 18:17:26');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300002, 100019, 2, 2798, 0, '2018-04-11 18:18:17');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300002, 100018, 1, 216, 0, '2018-04-11 18:18:17');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300003, 100025, 1, 150, 3, '2018-04-11 18:26:16');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300003, 100024, 1, 1299, 3, '2018-04-11 18:26:16');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300004, 100027, 1, 99, 0, '2018-04-11 18:27:06');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300004, 100026, 1, 29999, 0, '2018-04-11 18:27:06');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300005, 100025, 1, 150, 0, '2018-04-11 18:34:44');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300005, 100024, 1, 1299, 0, '2018-04-11 18:34:44');
INSERT INTO fleastreet.order_item (order_id, product_id, product_quantity, sub_total, state, create_time) VALUES (300006, 100025, 1, 150, 0, '2018-04-12 11:48:54');

-- 订单评价表 id从500000递增。查询产品对应评价，关联order_item和order_review表
drop table IF EXISTS `order_review`;
create table `order_review`(
  order_review_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '评价id',
  order_item_id BIGINT NOT NULL COMMENT '订单项id',
  review_content VARCHAR(1000) NOT NULL DEFAULT '好评' COMMENT '评价内容',
  star TINYINT NOT NULL DEFAULT 5 COMMENT '星级',
  state INT DEFAULT 1000  COMMENT '状态标志,1000 在用 1100 失效',
  create_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (order_review_id),
  key idx_review_order_id(order_item_id),
  UNIQUE key uni_order_item_id(order_item_id),
  CONSTRAINT `fk_order_item_id` FOREIGN KEY (`order_item_id`) REFERENCES `order_item`(`order_item_id`),
  CONSTRAINT `fk_review_state` FOREIGN KEY (`state`) REFERENCES `state`(`state_id`)
)ENgine=innoDB DEFAULT  CHARSET = utf8  AUTO_INCREMENT=500000 comment='记录订单评价';







